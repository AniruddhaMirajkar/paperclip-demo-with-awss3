json.extract! blog, :id, :blog_title, :blog_body, :created_at, :updated_at
json.url blog_url(blog, format: :json)
