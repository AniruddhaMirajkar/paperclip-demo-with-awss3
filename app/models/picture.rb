class Picture < ApplicationRecord
  belongs_to :post

  has_attached_file :image,
    storage: :s3,
    :url => ':s3_alias_url',
    :s3_host_alias => "Enter Your ColudFront URL",
    path: "posts/pictures/:id/:style/:filename",
    s3_credentials: {
      bucket: "Enter Your bucket name",
      access_key_id: "Enter Your access_key_id",
      secret_access_key: "Enter Your secret_access_key",
      s3_region: "Enter Your s3_region"
    },
    s3_permissions: {
      original: "public-read"
    }
  do_not_validate_attachment_file_type :image
end
