class CreateBlogs < ActiveRecord::Migration[6.0]
  def change
    create_table :blogs do |t|
      t.string :blog_title
      t.string :blog_body

      t.timestamps
    end
  end
end
