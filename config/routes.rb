Rails.application.routes.draw do
  resources :blogs
	root 'pages#home'
  resources :posts
  post "/posts/upload_image/:id", to: "posts#upload_image", as: :upload_post_image 
  post "/blogs/upload_image/:id", to: "blogs#upload_image", as: :upload_blog_image 
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
